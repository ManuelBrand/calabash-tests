# README #

This Repo contains code used for the project MoreApp Integration Testing.

### What is this repository for? ###

* Calabash Testsuite for IntegrationApp (https://bitbucket.org/ManuelBrand/integrationapp/overview)
* v0.1

### How do I get set up? ###

* -- Build-scripts will follow soon
* Dependencies:
* Calabash (calaba.sh)
* Ruby


Manuel Brand student at HAN 474283