# Sign app:
# calabash-android resign /Users/manbrand/Workspaces/Tests/IntegrationApp/platforms/android/build/outputs/apk/android-debug.apk

Feature: Test Full App

  Scenario: I can click the Alertbutton so i see an Alert
    Given I am on the first page
    When I press button with id "basicAlertButton"
    Then I wait to see the Alert
    Then I press Ok
    Then the Alert should be gone and i am back on the first page

  Scenario: I can move to the Modal Page using the SideMenu and click the ModalButton so i see a Modal
    Given I am on the start page
    When I press the button with id "menuItem"
    Then I see the side menu show up
    When I press the button with id "modalPage"
    Then I see the page Modal Page
    When I press the button with id "modalButton"
    Then I see a Modal show up
    When I press the button with id "closeModalButton"
    Then I am back on the Modal Page

  Scenario: I can move to the ActionSheet Page using the SideMenu and click the ActionsheetButton so i see an ActionSheet
    Given I am on the landing page
    When I press the button with the id "menuItem"
    Then I see the side menu appear
    When I press the button with id "ActionSheetPage"
    Then I see the page ActionSheet page
    When I press the button with id "basicActionSheetButton"
    Then I see an ActionSheet show up
    When I press the button with "Option 1"
    Then The sheet will disappear

  Scenario: I can move to the Login Page using the SideMenu and request a new password
    Given I am on the starting page
    When I Press the "menuItem" button
    Then I can see the side menu appear
    When I Press the button with id "loginPage"
    Then I see the page Login page
    When I press the button "Forgot Password"
    Then An Alert will show up
    When I fill in my email
    And Press send
    Then The Alert will disappear
