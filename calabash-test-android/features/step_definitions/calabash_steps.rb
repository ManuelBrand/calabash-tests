require 'calabash-android/calabash_steps'
#
# Alert feature
#
Given(/^I am on the first page$/) do
  sleep(1)
  element_exists("systemWebview css:'*' {textContent CONTAINS 'Home'} {nodeName CONTAINS 'ION-TITLE'}")
end
When(/^I press button with id "([^\"]*)"$/) do |basicAlertButton|
  sleep(1)
  touch("systemWebview css:'*' {id CONTAINS 'basicAlertButton'}")
end
Then(/I wait to see the Alert/) do
  sleep(1)
  element_exists("systemWebview css:'.alert-message'")
end
Then(/I press Ok$/) do
  sleep(1)
  touch("systemWebview css:'*' {nodeName CONTAINS 'BUTTON'} {textContent CONTAINS 'Ok'}")
end
Then(/the Alert should be gone and i am back on the first page$/) do
  sleep(1)
  element_exists("systemWebview css:'*' {textContent CONTAINS 'Home'} {nodeName CONTAINS 'ION-TITLE'}")
end

#
# MoveToModalPage and OpenModal feature
#
Given(/I am on the start page/) do
  sleep(1)
  element_exists("systemWebview css:'*' {textContent CONTAINS 'Home'} {nodeName CONTAINS 'ION-TITLE'}")
end
When(/I press the button with id "menuItem"/) do
  touch("systemWebview css:'#menuButton'")
end
Then(/I see the side menu show up/) do
  sleep(1)
  element_exists("systemWebview css:'*' {textContent CONTAINS 'Menu'} {nodeName CONTAINS 'ION-TITLE'}")
end
When(/I press the button with id "modalPage"/) do
  sleep(1)
  touch("systemWebview css:'*' {textContent CONTAINS 'Modal'} {nodeName CONTAINS 'BUTTON'}")
end
Then(/I see the page Modal Page/) do
  sleep(1)
  element_exists("systemWebview css:'*' {textContent CONTAINS 'Modal Page'} {nodeName CONTAINS 'ION-TITLE'}")
end
When(/^I press the button with id "modalButton"/) do
  sleep(1)
  touch("systemWebview css:'*' {nodeName CONTAINS 'BUTTON'} {textContent CONTAINS 'Modal'}")
end
Then(/I see a Modal show up/) do
  sleep(1)
  element_exists("systemWebview css:'*' {textContent CONTAINS 'Basic Modal'} {nodeName CONTAINS 'H2'}")
end
Then(/I press the button with id "closeModalButton"/) do
  sleep(1)
  touch("systemWebview css:'*' {id CONTAINS 'closeModalButton'}")
end
Then(/I am back on the Modal Page/) do
  sleep(1)
  element_exists("systemWebview css:'*' {textContent CONTAINS 'Home'} {nodeName CONTAINS 'ION-TITLE'}")
end

#
#MoveToActionSheetPage and OpenActionSheet feature
#
Given(/I am on the landing page/) do
  sleep(1)
  element_exists("systemWebview css:'*' {textContent CONTAINS 'Home'} {nodeName CONTAINS 'ION-TITLE'}")
end
When(/I press the button with the id "menuItem"/) do
  sleep(1)
  touch("systemWebview css:'#menuButton'")
end
Then(/I see the side menu appear/) do
  sleep(1)
  element_exists("systemWebview css:'*' {textContent CONTAINS 'Menu'} {nodeName CONTAINS 'ION-TITLE'}")
end
When(/I press the button with id "ActionSheetPage"/) do
  sleep(1)
  touch("systemWebview css:'*' {nodeName CONTAINS 'BUTTON'} {textContent CONTAINS 'Action'}")
end
Then(/I see the page ActionSheet page/) do
  sleep(1)
  element_exists("systemWebview css:'*' {textContent CONTAINS 'Actionsheet Page'} {nodeName CONTAINS 'ION-TITLE'}")
end
When(/I press the button with id "basicActionSheetButton"/) do
  sleep(1)
  touch("systemWebview css:'*' {id CONTAINS 'basicActionsheetButton'}")
end
Then(/I see an ActionSheet show up/) do
  sleep(1)
  element_exists("systemWebview css:'*' {class CONTAINS 'action-sheet-container'}")
end
When(/I press the button with "Option 1"/) do
  sleep(1)
  touch("systemWebview css:'*' {nodeName CONTAINS 'BUTTON'} {textContent CONTAINS 'Option 1'}")
end
Then(/The sheet will disappear/) do
  sleep(1)
  element_does_not_exist("systemWebview css:'*' {class CONTAINS 'action-sheet-container'}")
end

#
#
#
Given(/I am on the starting page/) do
  sleep(1)
  element_exists("systemWebview css:'*' {textContent CONTAINS 'Home'} {nodeName CONTAINS 'ION-TITLE'}")
end
When(/I Press the "menuItem" button/) do
  sleep(1)
  touch("systemWebview css:'#menuButton'")
end
Then(/I can see the side menu appear/) do
  sleep(1)
  element_exists("systemWebview css:'*' {textContent CONTAINS 'Menu'} {nodeName CONTAINS 'ION-TITLE'}")
end
When(/I Press the button with id "loginPage"/) do
  sleep(1)
  touch("systemWebview css:'*' {textContent CONTAINS 'Login'} {nodeName CONTAINS 'BUTTON'}")
end
Then(/I see the page Login page/) do
  sleep(1)
  element_exists("systemWebview css:'*' {textContent CONTAINS 'Login Page'} {nodeName CONTAINS 'ION-TITLE'}")
end
When(/I press the button "Forgot Password"/) do
  sleep(1)
  touch("systemWebview css:'#forgotPasswordButton'")
end
Then(/An Alert will show up/) do
  sleep(1)
  element_exists("systemWebview css:'.alert-message'")
end
When(/I fill in my email/) do
  wait_for_keyboard()
  keyboard_enter_text("email@adress.com")
end
And(/Press send/) do
  sleep(1)
  ("systemWebview css:'*' {textContent CONTAINS 'Send'} {nodeName CONTAINS 'BUTTON'}")
end
Then(/The Alert will disappear/) do
  element_does_not_exist("systemWebview css:'.alert-message'")
end
